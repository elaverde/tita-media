
import {Search} from '../model/search';
class All{
     constructor(){
        this.images = [];
        this.buscador = new Search();
       
   
    
    }
    async show_all(section){
        let all =["branding","web","photography","app"];

        for(let i = 0; i < all.length; i++){
           this.buscador.set_word_search(all[i]);
           let aux_images= await this.get_posts_all();
           if(aux_images){
             this.images=this.images.concat(aux_images);
           }
        }

    
        let innerHTML="<div class='grid'>";
        this.images.map((item,index) => {
         
            innerHTML += `<div style="background-image: url('${item.urls.small}')" id="item-${index+1}">
            <div class='show-msg'>
            <h1>CREATIVE LOGO</h1>
            <hr>
            <p>All</p>
            </div>
            </div>`;
        });
        section.innerHTML = innerHTML+"</div>";
        console.log(innerHTML);

     
       
    

    }
    get_posts_all(){
        return this.buscador.search().then(data => { 
             return data;
        });
    }    
}
export {All};