import App from './app'
import Branding from './branding'
import Photography from './photography'
import Web from './web'
import {All} from './all'


const pages={
    app: App,
    branding: Branding,
    photography: Photography,
    web: Web,
    all: new All()


};
export { pages};
