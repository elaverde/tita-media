import view from '../views/web.html'
import {Search} from '../model/search';
export default ()=>{
    const div = document.createElement('div');
    div.innerHTML = view;
    div.classList='grid';

    let search = new Search();
    search.set_word_search('web');
    search.get_posts().then(data => {
        data.map((item,index) => {
            console.log(item.urls.thumb);
            div.innerHTML += `<div style="background-image: url('${item.urls.small}')" id="item-${index+1}">
            <div class='show-msg'>
                <h1>CREATIVE LOGO</h1>
                <hr>
                <p>web</p>
            </div>
            </div>`;
        })
    })
    return div;
};
