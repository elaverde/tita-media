const clientId="DetHBKI3Rs-gNATnHnSKebYZLn9kakg63ZE6GbL598I";
const endpoint ="https://api.unsplash.com/search/photos"
class Search{
    constructor(){
        this.word_search = "" ;
        this.images=[];
    }
    set_word_search(word){
        this.word_search = word;
    }
    async get_posts(){
        this.images = await this.search();
        return this.images;
    }
    search(){
        return fetch(`${endpoint}?query=${this.word_search}&client_id=${clientId}`)
        .then(response => response.json())
        .then(data => {
           
            return data.results;
        })
        return f;
    }
    
}
export {Search};