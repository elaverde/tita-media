class Events{
    constructor(){
        this.view2 = document.querySelectorAll("#view-2");
    }
    toInit(){
        document.getElementById("view-1").addEventListener('click', () => {
            document.querySelector('#sections div').classList.add('grid-cubic');
            document.querySelector('#sections div').classList.remove('grid');
        });
        document.getElementById("view-2").addEventListener('click', () => {
            document.querySelector('#sections div').classList.add('grid');
            document.querySelector('#sections div').classList.remove('grid-cubic');
        });
    }
    
}
export {Events};