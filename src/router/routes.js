import {
    pages
} from '../controllers/main';
const section = document.getElementById('sections');
class Menu {
    constructor() {
        this.menu = document.querySelectorAll(".menu li a");
    }
    clean_items() {
        this.menu.forEach(item => {
            item.classList.remove("active");
        });
    }
    active_item(item) {
        this.clean_items();
        document.querySelectorAll("a[href='" + item + "']").forEach(item => {
            item.classList.add("active");
        })
    }
}
const router = (route) => {
    section.innerHTML = "";
    //limipiamos los links
    let menu = new Menu();
    menu.active_item(route);
    switch (route) {
        case '#all':
            let all = pages.all;
            let html = all.show_all(section);
            break;
        case '#branding': {
            return section.appendChild(pages.branding());
        }
        case '#web': {
            return section.appendChild(pages.web());
        }
        case '#photography': {
            return section.appendChild(pages.photography());
        }
        case '#app': {
            return section.appendChild(pages.app());
        }
        default:
            console.log('Pagina no encontrada 404');
    }
}
export {
    router
};