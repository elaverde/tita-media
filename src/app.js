

import './main.scss'
import {router} from './router/routes';
import {Events} from './model/events';

let events = new Events();
router(window.location.hash);
window.addEventListener('hashchange', () => {
    router(window.location.hash)
});
events.toInit();
