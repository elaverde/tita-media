const HTMWebpackPlugin = require('html-webpack-plugin');
module.exports = { 
    entry: './src/app.js',
    output: {
        path: __dirname + '/dist',
        filename: 'bundle.js'
    },
    module:{
        rules:[
            /*{
                test: /.css$/i,
                use: ['style-loader','css-loader']
            },*/
            {
                test: /\.s[ac]ss$/i,
                use:[
                    'style-loader',
                    'css-loader',
                    'sass-loader'
                ]
            },
            {
                test: /\.html$/i,
                use:["html-loader"]
            }
        ]
    },
    
    plugins: [
        new HTMWebpackPlugin({
            template: './src/index.html'
        })
    ],
    mode: 'development'

}