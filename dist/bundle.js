/*
 * ATTENTION: The "eval" devtool has been used (maybe by default in mode: "development").
 * This devtool is neither made for production nor for readable output files.
 * It uses "eval()" calls to create a separate source file in the browser devtools.
 * If you are trying to read the output file, select a different devtool (https://webpack.js.org/configuration/devtool/)
 * or disable the default devtool with "devtool: false".
 * If you are looking for production-ready output files, see mode: "production" (https://webpack.js.org/configuration/mode/).
 */
/******/ (() => { // webpackBootstrap
/******/ 	"use strict";
/******/ 	var __webpack_modules__ = ({

/***/ "./node_modules/css-loader/dist/cjs.js!./node_modules/sass-loader/dist/cjs.js!./src/main.scss":
/*!****************************************************************************************************!*\
  !*** ./node_modules/css-loader/dist/cjs.js!./node_modules/sass-loader/dist/cjs.js!./src/main.scss ***!
  \****************************************************************************************************/
/***/ ((module, __webpack_exports__, __webpack_require__) => {

eval("__webpack_require__.r(__webpack_exports__);\n/* harmony export */ __webpack_require__.d(__webpack_exports__, {\n/* harmony export */   \"default\": () => (__WEBPACK_DEFAULT_EXPORT__)\n/* harmony export */ });\n/* harmony import */ var _node_modules_css_loader_dist_runtime_noSourceMaps_js__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../node_modules/css-loader/dist/runtime/noSourceMaps.js */ \"./node_modules/css-loader/dist/runtime/noSourceMaps.js\");\n/* harmony import */ var _node_modules_css_loader_dist_runtime_noSourceMaps_js__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_node_modules_css_loader_dist_runtime_noSourceMaps_js__WEBPACK_IMPORTED_MODULE_0__);\n/* harmony import */ var _node_modules_css_loader_dist_runtime_api_js__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../node_modules/css-loader/dist/runtime/api.js */ \"./node_modules/css-loader/dist/runtime/api.js\");\n/* harmony import */ var _node_modules_css_loader_dist_runtime_api_js__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(_node_modules_css_loader_dist_runtime_api_js__WEBPACK_IMPORTED_MODULE_1__);\n/* harmony import */ var _node_modules_css_loader_dist_runtime_getUrl_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../node_modules/css-loader/dist/runtime/getUrl.js */ \"./node_modules/css-loader/dist/runtime/getUrl.js\");\n/* harmony import */ var _node_modules_css_loader_dist_runtime_getUrl_js__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(_node_modules_css_loader_dist_runtime_getUrl_js__WEBPACK_IMPORTED_MODULE_2__);\n// Imports\n\n\n\nvar ___CSS_LOADER_URL_IMPORT_0___ = new URL(/* asset import */ __webpack_require__(/*! ./images/search_icon.png */ \"./src/images/search_icon.png\"), __webpack_require__.b);\nvar ___CSS_LOADER_EXPORT___ = _node_modules_css_loader_dist_runtime_api_js__WEBPACK_IMPORTED_MODULE_1___default()((_node_modules_css_loader_dist_runtime_noSourceMaps_js__WEBPACK_IMPORTED_MODULE_0___default()));\n___CSS_LOADER_EXPORT___.push([module.id, \"@import url(https://fonts.googleapis.com/css2?family=Montserrat:wght@100&display=swap);\"]);\n___CSS_LOADER_EXPORT___.push([module.id, \"@import url(https://fonts.googleapis.com/css2?family=Montserrat:wght@100;500&display=swap);\"]);\nvar ___CSS_LOADER_URL_REPLACEMENT_0___ = _node_modules_css_loader_dist_runtime_getUrl_js__WEBPACK_IMPORTED_MODULE_2___default()(___CSS_LOADER_URL_IMPORT_0___);\n// Module\n___CSS_LOADER_EXPORT___.push([module.id, \"body {\\n  background-color: #FFF;\\n  width: 100%;\\n  margin: 0 auto;\\n  display: flex;\\n  flex-direction: column; }\\n\\n.header {\\n  display: flex;\\n  flex-direction: row;\\n  align-items: center;\\n  justify-content: space-between;\\n  height: 80px;\\n  margin: 0 10% 0 10%; }\\n  .header .logo {\\n    display: flex;\\n    flex-direction: row;\\n    align-items: center; }\\n    .header .logo .title {\\n      display: flex;\\n      flex-direction: column; }\\n      .header .logo .title h1 {\\n        font-size: 10px;\\n        font-weight: bold;\\n        color: #707070;\\n        padding: 0;\\n        margin: 0; }\\n      .header .logo .title p {\\n        font-size: 5px;\\n        color: #707070;\\n        padding: 0;\\n        margin: 0; }\\n\\nul {\\n  display: flex;\\n  flex-direction: row;\\n  justify-items: center;\\n  align-items: center; }\\n  ul li {\\n    display: inline-block;\\n    min-width: 30px;\\n    text-align: center;\\n    margin: 0 10px 0 10px; }\\n    ul li a {\\n      padding: 7px 25px 7px 25px;\\n      width: 100%;\\n      height: 100%;\\n      color: #707070;\\n      text-decoration: none;\\n      font-family: 'Montserrat', sans-serif;\\n      font-size: .6em;\\n      font-weight: bold; }\\n    ul li .active {\\n      background-color: #fc758c;\\n      color: #FFF; }\\n  ul input[type=search] {\\n    background: url(\" + ___CSS_LOADER_URL_REPLACEMENT_0___ + \") no-repeat 9px center;\\n    border: 0;\\n    padding: 5px 10px 5px 32px;\\n    width: 55px;\\n    -webkit-transition: all .5s;\\n    -moz-transition: all .5s;\\n    transition: all .5s; }\\n    ul input[type=search]:focus {\\n      width: 130px;\\n      border-bottom: solid 1px #ccc;\\n      outline: none; }\\n\\n.landing {\\n  min-width: 100%;\\n  display: flex;\\n  background-color: #fc758c;\\n  flex-direction: column;\\n  align-items: center; }\\n  .landing .title {\\n    display: flex;\\n    flex-direction: column;\\n    margin: 10% 0 0 0;\\n    font-family: 'Montserrat', sans-serif; }\\n    .landing .title h1 {\\n      font-size: 2em;\\n      font-weight: bold;\\n      color: #FFF;\\n      padding-bottom: 1em;\\n      margin: 0;\\n      text-align: center;\\n      letter-spacing: 10px; }\\n    .landing .title p {\\n      font-size: 0.7em;\\n      color: #FFF;\\n      padding: 0;\\n      margin: 0;\\n      padding-bottom: 1em;\\n      text-align: center; }\\n\\n.btn-view {\\n  margin: 2em 0 10% 0;\\n  padding: 4px 12px 4px 12px;\\n  font-size: 8px;\\n  width: auto;\\n  background-color: #FFF;\\n  color: #fc758c;\\n  border: 0;\\n  cursor: pointer; }\\n\\n.partfolio {\\n  display: inline-flex;\\n  justify-content: flex-start;\\n  align-items: center;\\n  flex-direction: column; }\\n  .partfolio .view-navigation {\\n    display: flex;\\n    justify-content: center;\\n    padding: 2em 0 2em 0; }\\n    .partfolio .view-navigation li {\\n      width: 5px;\\n      padding: 0;\\n      margin: 0;\\n      cursor: pointer; }\\n\\n.sections {\\n  width: 80%;\\n  min-height: auto;\\n  display: block; }\\n\\n.grid-cubic {\\n  display: grid;\\n  grid-template-columns: repeat(12, 1fr);\\n  grid-template-rows: repeat(15, 1fr);\\n  grid-gap: .5rem;\\n  padding: .5rem; }\\n  .grid-cubic div {\\n    background-color: #d5d5d5;\\n    grid-column: span 4;\\n    grid-row: span 4;\\n    background-size: cover;\\n    min-height: 200px;\\n    transition: all .5s; }\\n  .grid-cubic div:hover div {\\n    opacity: .8; }\\n\\n.show-msg {\\n  background-color: #fc758c !important;\\n  opacity: 0;\\n  width: 100%;\\n  height: 100%;\\n  display: flex;\\n  flex-direction: column;\\n  justify-content: center;\\n  align-items: center;\\n  color: #ffffff;\\n  cursor: pointer; }\\n  .show-msg hr {\\n    width: 70%;\\n    border: 0;\\n    height: 1px;\\n    background-color: #ffffff; }\\n  .show-msg h1 {\\n    font-size: 2em;\\n    font-weight: 300;\\n    padding: 0;\\n    margin: 0;\\n    font-family: 'Montserrat', sans-serif; }\\n  .show-msg p {\\n    font-size: 1em;\\n    text-transform: capitalize;\\n    font-family: 'Montserrat', sans-serif; }\\n\\n.grid {\\n  display: grid;\\n  grid-template-columns: repeat(12, 1fr);\\n  grid-template-rows: repeat(15, 1fr);\\n  grid-gap: .5rem;\\n  padding: .5rem;\\n  grid-auto-rows: minmax(100px, auto);\\n  min-height: calc(100vh - 1rem); }\\n  .grid div {\\n    background-color: #d5d5d5;\\n    grid-column: span 4;\\n    background-size: cover;\\n    min-height: 200px; }\\n  .grid #item-1 {\\n    grid-column: 1/5;\\n    grid-row: 1/4; }\\n  .grid #item-2 {\\n    grid-column: 5/9;\\n    grid-row: 1/3; }\\n  .grid #item-3 {\\n    grid-column: 9/13;\\n    grid-row: 1/6; }\\n  .grid #item-4 {\\n    grid-column: 1/5;\\n    grid-row: 4/10; }\\n  .grid #item-5 {\\n    grid-column: 5/9;\\n    grid-row: 3/8; }\\n  .grid #item-6 {\\n    grid-column: 9/13;\\n    grid-row: 6/9; }\\n  .grid #item-7 {\\n    grid-column: 1/5;\\n    grid-row: 10/15; }\\n  .grid #item-8 {\\n    grid-column: 5/9;\\n    grid-row: 8/15; }\\n  .grid #item-9 {\\n    grid-column: 9/13;\\n    grid-row: 9/12; }\\n  .grid #item-10 {\\n    grid-column: 9/13;\\n    grid-row: 12/15; }\\n  .grid div {\\n    background-color: #d5d5d5;\\n    grid-column: span 4;\\n    grid-row: span 4;\\n    background-size: cover;\\n    min-height: 200px;\\n    transition: all .5s; }\\n  .grid div:hover div {\\n    opacity: .8; }\\n\\n.footer {\\n  display: flex;\\n  flex-direction: column;\\n  align-items: center;\\n  justify-content: center;\\n  font-family: 'Montserrat', sans-serif;\\n  font-size: .6em;\\n  font-weight: bold;\\n  color: #707070; }\\n  .footer div {\\n    padding: 5px 0 10px 0; }\\n  .footer button {\\n    background-color: #fc758c;\\n    border: 0;\\n    padding: 5px 20px 5px 20px;\\n    color: #FFF;\\n    font-size: .5rem;\\n    letter-spacing: 2px;\\n    cursor: pointer; }\\n  .footer a {\\n    text-decoration: none;\\n    color: #fc758c; }\\n  .footer ul {\\n    padding: 0;\\n    margin: 0;\\n    display: flex;\\n    justify-content: center;\\n    align-items: center; }\\n    .footer ul li {\\n      padding: 0;\\n      margin: 0;\\n      min-width: 10px; }\\n    .footer ul a {\\n      padding: 8px; }\\n\\n@media (max-width: 768px) {\\n  .header {\\n    display: flex;\\n    flex-direction: column;\\n    height: auto;\\n    margin: 0;\\n    padding: 0; }\\n  ul {\\n    display: flex;\\n    flex-direction: column;\\n    justify-items: center;\\n    align-items: center;\\n    width: 100%;\\n    margin: 0;\\n    padding: 0; }\\n    ul li {\\n      width: 100%;\\n      margin: 0;\\n      padding: 0;\\n      display: block;\\n      padding: 10px 0 10px 0; }\\n      ul li a {\\n        display: block;\\n        width: 100% !important;\\n        margin: 0;\\n        padding: 10px 0 0 0; }\\n  .navigation .menu {\\n    display: none; }\\n  .navigation .view-navigation {\\n    display: none !important; }\\n  .grid,\\n  .grid-cubic {\\n    width: 100%;\\n    display: flex;\\n    flex-direction: column; }\\n    .grid div,\\n    .grid-cubic div {\\n      width: 100%;\\n      max-height: 300px; }\\n  .footer {\\n    display: flex;\\n    flex-direction: column;\\n    align-items: center;\\n    justify-content: center;\\n    height: auto; }\\n    .footer div {\\n      padding: 10px 0 10px 0; }\\n    .footer button {\\n      margin-top: 100px; }\\n    .footer ul {\\n      padding: 0;\\n      margin: 0;\\n      display: flex;\\n      justify-content: center;\\n      flex-direction: row;\\n      align-items: center; }\\n      .footer ul li {\\n        padding: 0;\\n        margin: 0;\\n        min-width: 10px; }\\n      .footer ul a {\\n        padding: 8px; } }\\n\", \"\"]);\n// Exports\n/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (___CSS_LOADER_EXPORT___);\n\n\n//# sourceURL=webpack://Tita/./src/main.scss?./node_modules/css-loader/dist/cjs.js!./node_modules/sass-loader/dist/cjs.js");

/***/ }),

/***/ "./node_modules/css-loader/dist/runtime/api.js":
/*!*****************************************************!*\
  !*** ./node_modules/css-loader/dist/runtime/api.js ***!
  \*****************************************************/
/***/ ((module) => {

eval("\n\n/*\n  MIT License http://www.opensource.org/licenses/mit-license.php\n  Author Tobias Koppers @sokra\n*/\nmodule.exports = function (cssWithMappingToString) {\n  var list = []; // return the list of modules as css string\n\n  list.toString = function toString() {\n    return this.map(function (item) {\n      var content = \"\";\n      var needLayer = typeof item[5] !== \"undefined\";\n\n      if (item[4]) {\n        content += \"@supports (\".concat(item[4], \") {\");\n      }\n\n      if (item[2]) {\n        content += \"@media \".concat(item[2], \" {\");\n      }\n\n      if (needLayer) {\n        content += \"@layer\".concat(item[5].length > 0 ? \" \".concat(item[5]) : \"\", \" {\");\n      }\n\n      content += cssWithMappingToString(item);\n\n      if (needLayer) {\n        content += \"}\";\n      }\n\n      if (item[2]) {\n        content += \"}\";\n      }\n\n      if (item[4]) {\n        content += \"}\";\n      }\n\n      return content;\n    }).join(\"\");\n  }; // import a list of modules into the list\n\n\n  list.i = function i(modules, media, dedupe, supports, layer) {\n    if (typeof modules === \"string\") {\n      modules = [[null, modules, undefined]];\n    }\n\n    var alreadyImportedModules = {};\n\n    if (dedupe) {\n      for (var k = 0; k < this.length; k++) {\n        var id = this[k][0];\n\n        if (id != null) {\n          alreadyImportedModules[id] = true;\n        }\n      }\n    }\n\n    for (var _k = 0; _k < modules.length; _k++) {\n      var item = [].concat(modules[_k]);\n\n      if (dedupe && alreadyImportedModules[item[0]]) {\n        continue;\n      }\n\n      if (typeof layer !== \"undefined\") {\n        if (typeof item[5] === \"undefined\") {\n          item[5] = layer;\n        } else {\n          item[1] = \"@layer\".concat(item[5].length > 0 ? \" \".concat(item[5]) : \"\", \" {\").concat(item[1], \"}\");\n          item[5] = layer;\n        }\n      }\n\n      if (media) {\n        if (!item[2]) {\n          item[2] = media;\n        } else {\n          item[1] = \"@media \".concat(item[2], \" {\").concat(item[1], \"}\");\n          item[2] = media;\n        }\n      }\n\n      if (supports) {\n        if (!item[4]) {\n          item[4] = \"\".concat(supports);\n        } else {\n          item[1] = \"@supports (\".concat(item[4], \") {\").concat(item[1], \"}\");\n          item[4] = supports;\n        }\n      }\n\n      list.push(item);\n    }\n  };\n\n  return list;\n};\n\n//# sourceURL=webpack://Tita/./node_modules/css-loader/dist/runtime/api.js?");

/***/ }),

/***/ "./node_modules/css-loader/dist/runtime/getUrl.js":
/*!********************************************************!*\
  !*** ./node_modules/css-loader/dist/runtime/getUrl.js ***!
  \********************************************************/
/***/ ((module) => {

eval("\n\nmodule.exports = function (url, options) {\n  if (!options) {\n    options = {};\n  }\n\n  if (!url) {\n    return url;\n  }\n\n  url = String(url.__esModule ? url.default : url); // If url is already wrapped in quotes, remove them\n\n  if (/^['\"].*['\"]$/.test(url)) {\n    url = url.slice(1, -1);\n  }\n\n  if (options.hash) {\n    url += options.hash;\n  } // Should url be wrapped?\n  // See https://drafts.csswg.org/css-values-3/#urls\n\n\n  if (/[\"'() \\t\\n]|(%20)/.test(url) || options.needQuotes) {\n    return \"\\\"\".concat(url.replace(/\"/g, '\\\\\"').replace(/\\n/g, \"\\\\n\"), \"\\\"\");\n  }\n\n  return url;\n};\n\n//# sourceURL=webpack://Tita/./node_modules/css-loader/dist/runtime/getUrl.js?");

/***/ }),

/***/ "./node_modules/css-loader/dist/runtime/noSourceMaps.js":
/*!**************************************************************!*\
  !*** ./node_modules/css-loader/dist/runtime/noSourceMaps.js ***!
  \**************************************************************/
/***/ ((module) => {

eval("\n\nmodule.exports = function (i) {\n  return i[1];\n};\n\n//# sourceURL=webpack://Tita/./node_modules/css-loader/dist/runtime/noSourceMaps.js?");

/***/ }),

/***/ "./src/views/app.html":
/*!****************************!*\
  !*** ./src/views/app.html ***!
  \****************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

eval("__webpack_require__.r(__webpack_exports__);\n/* harmony export */ __webpack_require__.d(__webpack_exports__, {\n/* harmony export */   \"default\": () => (__WEBPACK_DEFAULT_EXPORT__)\n/* harmony export */ });\n// Module\nvar code = \"\";\n// Exports\n/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (code);\n\n//# sourceURL=webpack://Tita/./src/views/app.html?");

/***/ }),

/***/ "./src/views/branding.html":
/*!*********************************!*\
  !*** ./src/views/branding.html ***!
  \*********************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

eval("__webpack_require__.r(__webpack_exports__);\n/* harmony export */ __webpack_require__.d(__webpack_exports__, {\n/* harmony export */   \"default\": () => (__WEBPACK_DEFAULT_EXPORT__)\n/* harmony export */ });\n// Module\nvar code = \"\";\n// Exports\n/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (code);\n\n//# sourceURL=webpack://Tita/./src/views/branding.html?");

/***/ }),

/***/ "./src/views/web.html":
/*!****************************!*\
  !*** ./src/views/web.html ***!
  \****************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

eval("__webpack_require__.r(__webpack_exports__);\n/* harmony export */ __webpack_require__.d(__webpack_exports__, {\n/* harmony export */   \"default\": () => (__WEBPACK_DEFAULT_EXPORT__)\n/* harmony export */ });\n// Module\nvar code = \"\";\n// Exports\n/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (code);\n\n//# sourceURL=webpack://Tita/./src/views/web.html?");

/***/ }),

/***/ "./src/main.scss":
/*!***********************!*\
  !*** ./src/main.scss ***!
  \***********************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

eval("__webpack_require__.r(__webpack_exports__);\n/* harmony export */ __webpack_require__.d(__webpack_exports__, {\n/* harmony export */   \"default\": () => (__WEBPACK_DEFAULT_EXPORT__)\n/* harmony export */ });\n/* harmony import */ var _node_modules_style_loader_dist_runtime_injectStylesIntoStyleTag_js__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! !../node_modules/style-loader/dist/runtime/injectStylesIntoStyleTag.js */ \"./node_modules/style-loader/dist/runtime/injectStylesIntoStyleTag.js\");\n/* harmony import */ var _node_modules_style_loader_dist_runtime_injectStylesIntoStyleTag_js__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_node_modules_style_loader_dist_runtime_injectStylesIntoStyleTag_js__WEBPACK_IMPORTED_MODULE_0__);\n/* harmony import */ var _node_modules_style_loader_dist_runtime_styleDomAPI_js__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! !../node_modules/style-loader/dist/runtime/styleDomAPI.js */ \"./node_modules/style-loader/dist/runtime/styleDomAPI.js\");\n/* harmony import */ var _node_modules_style_loader_dist_runtime_styleDomAPI_js__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(_node_modules_style_loader_dist_runtime_styleDomAPI_js__WEBPACK_IMPORTED_MODULE_1__);\n/* harmony import */ var _node_modules_style_loader_dist_runtime_insertBySelector_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! !../node_modules/style-loader/dist/runtime/insertBySelector.js */ \"./node_modules/style-loader/dist/runtime/insertBySelector.js\");\n/* harmony import */ var _node_modules_style_loader_dist_runtime_insertBySelector_js__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(_node_modules_style_loader_dist_runtime_insertBySelector_js__WEBPACK_IMPORTED_MODULE_2__);\n/* harmony import */ var _node_modules_style_loader_dist_runtime_setAttributesWithoutAttributes_js__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! !../node_modules/style-loader/dist/runtime/setAttributesWithoutAttributes.js */ \"./node_modules/style-loader/dist/runtime/setAttributesWithoutAttributes.js\");\n/* harmony import */ var _node_modules_style_loader_dist_runtime_setAttributesWithoutAttributes_js__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(_node_modules_style_loader_dist_runtime_setAttributesWithoutAttributes_js__WEBPACK_IMPORTED_MODULE_3__);\n/* harmony import */ var _node_modules_style_loader_dist_runtime_insertStyleElement_js__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! !../node_modules/style-loader/dist/runtime/insertStyleElement.js */ \"./node_modules/style-loader/dist/runtime/insertStyleElement.js\");\n/* harmony import */ var _node_modules_style_loader_dist_runtime_insertStyleElement_js__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(_node_modules_style_loader_dist_runtime_insertStyleElement_js__WEBPACK_IMPORTED_MODULE_4__);\n/* harmony import */ var _node_modules_style_loader_dist_runtime_styleTagTransform_js__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! !../node_modules/style-loader/dist/runtime/styleTagTransform.js */ \"./node_modules/style-loader/dist/runtime/styleTagTransform.js\");\n/* harmony import */ var _node_modules_style_loader_dist_runtime_styleTagTransform_js__WEBPACK_IMPORTED_MODULE_5___default = /*#__PURE__*/__webpack_require__.n(_node_modules_style_loader_dist_runtime_styleTagTransform_js__WEBPACK_IMPORTED_MODULE_5__);\n/* harmony import */ var _node_modules_css_loader_dist_cjs_js_node_modules_sass_loader_dist_cjs_js_main_scss__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! !!../node_modules/css-loader/dist/cjs.js!../node_modules/sass-loader/dist/cjs.js!./main.scss */ \"./node_modules/css-loader/dist/cjs.js!./node_modules/sass-loader/dist/cjs.js!./src/main.scss\");\n\n      \n      \n      \n      \n      \n      \n      \n      \n      \n\nvar options = {};\n\noptions.styleTagTransform = (_node_modules_style_loader_dist_runtime_styleTagTransform_js__WEBPACK_IMPORTED_MODULE_5___default());\noptions.setAttributes = (_node_modules_style_loader_dist_runtime_setAttributesWithoutAttributes_js__WEBPACK_IMPORTED_MODULE_3___default());\n\n      options.insert = _node_modules_style_loader_dist_runtime_insertBySelector_js__WEBPACK_IMPORTED_MODULE_2___default().bind(null, \"head\");\n    \noptions.domAPI = (_node_modules_style_loader_dist_runtime_styleDomAPI_js__WEBPACK_IMPORTED_MODULE_1___default());\noptions.insertStyleElement = (_node_modules_style_loader_dist_runtime_insertStyleElement_js__WEBPACK_IMPORTED_MODULE_4___default());\n\nvar update = _node_modules_style_loader_dist_runtime_injectStylesIntoStyleTag_js__WEBPACK_IMPORTED_MODULE_0___default()(_node_modules_css_loader_dist_cjs_js_node_modules_sass_loader_dist_cjs_js_main_scss__WEBPACK_IMPORTED_MODULE_6__[\"default\"], options);\n\n\n\n\n       /* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (_node_modules_css_loader_dist_cjs_js_node_modules_sass_loader_dist_cjs_js_main_scss__WEBPACK_IMPORTED_MODULE_6__[\"default\"] && _node_modules_css_loader_dist_cjs_js_node_modules_sass_loader_dist_cjs_js_main_scss__WEBPACK_IMPORTED_MODULE_6__[\"default\"].locals ? _node_modules_css_loader_dist_cjs_js_node_modules_sass_loader_dist_cjs_js_main_scss__WEBPACK_IMPORTED_MODULE_6__[\"default\"].locals : undefined);\n\n\n//# sourceURL=webpack://Tita/./src/main.scss?");

/***/ }),

/***/ "./node_modules/style-loader/dist/runtime/injectStylesIntoStyleTag.js":
/*!****************************************************************************!*\
  !*** ./node_modules/style-loader/dist/runtime/injectStylesIntoStyleTag.js ***!
  \****************************************************************************/
/***/ ((module) => {

eval("\n\nvar stylesInDOM = [];\n\nfunction getIndexByIdentifier(identifier) {\n  var result = -1;\n\n  for (var i = 0; i < stylesInDOM.length; i++) {\n    if (stylesInDOM[i].identifier === identifier) {\n      result = i;\n      break;\n    }\n  }\n\n  return result;\n}\n\nfunction modulesToDom(list, options) {\n  var idCountMap = {};\n  var identifiers = [];\n\n  for (var i = 0; i < list.length; i++) {\n    var item = list[i];\n    var id = options.base ? item[0] + options.base : item[0];\n    var count = idCountMap[id] || 0;\n    var identifier = \"\".concat(id, \" \").concat(count);\n    idCountMap[id] = count + 1;\n    var indexByIdentifier = getIndexByIdentifier(identifier);\n    var obj = {\n      css: item[1],\n      media: item[2],\n      sourceMap: item[3],\n      supports: item[4],\n      layer: item[5]\n    };\n\n    if (indexByIdentifier !== -1) {\n      stylesInDOM[indexByIdentifier].references++;\n      stylesInDOM[indexByIdentifier].updater(obj);\n    } else {\n      var updater = addElementStyle(obj, options);\n      options.byIndex = i;\n      stylesInDOM.splice(i, 0, {\n        identifier: identifier,\n        updater: updater,\n        references: 1\n      });\n    }\n\n    identifiers.push(identifier);\n  }\n\n  return identifiers;\n}\n\nfunction addElementStyle(obj, options) {\n  var api = options.domAPI(options);\n  api.update(obj);\n\n  var updater = function updater(newObj) {\n    if (newObj) {\n      if (newObj.css === obj.css && newObj.media === obj.media && newObj.sourceMap === obj.sourceMap && newObj.supports === obj.supports && newObj.layer === obj.layer) {\n        return;\n      }\n\n      api.update(obj = newObj);\n    } else {\n      api.remove();\n    }\n  };\n\n  return updater;\n}\n\nmodule.exports = function (list, options) {\n  options = options || {};\n  list = list || [];\n  var lastIdentifiers = modulesToDom(list, options);\n  return function update(newList) {\n    newList = newList || [];\n\n    for (var i = 0; i < lastIdentifiers.length; i++) {\n      var identifier = lastIdentifiers[i];\n      var index = getIndexByIdentifier(identifier);\n      stylesInDOM[index].references--;\n    }\n\n    var newLastIdentifiers = modulesToDom(newList, options);\n\n    for (var _i = 0; _i < lastIdentifiers.length; _i++) {\n      var _identifier = lastIdentifiers[_i];\n\n      var _index = getIndexByIdentifier(_identifier);\n\n      if (stylesInDOM[_index].references === 0) {\n        stylesInDOM[_index].updater();\n\n        stylesInDOM.splice(_index, 1);\n      }\n    }\n\n    lastIdentifiers = newLastIdentifiers;\n  };\n};\n\n//# sourceURL=webpack://Tita/./node_modules/style-loader/dist/runtime/injectStylesIntoStyleTag.js?");

/***/ }),

/***/ "./node_modules/style-loader/dist/runtime/insertBySelector.js":
/*!********************************************************************!*\
  !*** ./node_modules/style-loader/dist/runtime/insertBySelector.js ***!
  \********************************************************************/
/***/ ((module) => {

eval("\n\nvar memo = {};\n/* istanbul ignore next  */\n\nfunction getTarget(target) {\n  if (typeof memo[target] === \"undefined\") {\n    var styleTarget = document.querySelector(target); // Special case to return head of iframe instead of iframe itself\n\n    if (window.HTMLIFrameElement && styleTarget instanceof window.HTMLIFrameElement) {\n      try {\n        // This will throw an exception if access to iframe is blocked\n        // due to cross-origin restrictions\n        styleTarget = styleTarget.contentDocument.head;\n      } catch (e) {\n        // istanbul ignore next\n        styleTarget = null;\n      }\n    }\n\n    memo[target] = styleTarget;\n  }\n\n  return memo[target];\n}\n/* istanbul ignore next  */\n\n\nfunction insertBySelector(insert, style) {\n  var target = getTarget(insert);\n\n  if (!target) {\n    throw new Error(\"Couldn't find a style target. This probably means that the value for the 'insert' parameter is invalid.\");\n  }\n\n  target.appendChild(style);\n}\n\nmodule.exports = insertBySelector;\n\n//# sourceURL=webpack://Tita/./node_modules/style-loader/dist/runtime/insertBySelector.js?");

/***/ }),

/***/ "./node_modules/style-loader/dist/runtime/insertStyleElement.js":
/*!**********************************************************************!*\
  !*** ./node_modules/style-loader/dist/runtime/insertStyleElement.js ***!
  \**********************************************************************/
/***/ ((module) => {

eval("\n\n/* istanbul ignore next  */\nfunction insertStyleElement(options) {\n  var element = document.createElement(\"style\");\n  options.setAttributes(element, options.attributes);\n  options.insert(element, options.options);\n  return element;\n}\n\nmodule.exports = insertStyleElement;\n\n//# sourceURL=webpack://Tita/./node_modules/style-loader/dist/runtime/insertStyleElement.js?");

/***/ }),

/***/ "./node_modules/style-loader/dist/runtime/setAttributesWithoutAttributes.js":
/*!**********************************************************************************!*\
  !*** ./node_modules/style-loader/dist/runtime/setAttributesWithoutAttributes.js ***!
  \**********************************************************************************/
/***/ ((module, __unused_webpack_exports, __webpack_require__) => {

eval("\n\n/* istanbul ignore next  */\nfunction setAttributesWithoutAttributes(styleElement) {\n  var nonce =  true ? __webpack_require__.nc : 0;\n\n  if (nonce) {\n    styleElement.setAttribute(\"nonce\", nonce);\n  }\n}\n\nmodule.exports = setAttributesWithoutAttributes;\n\n//# sourceURL=webpack://Tita/./node_modules/style-loader/dist/runtime/setAttributesWithoutAttributes.js?");

/***/ }),

/***/ "./node_modules/style-loader/dist/runtime/styleDomAPI.js":
/*!***************************************************************!*\
  !*** ./node_modules/style-loader/dist/runtime/styleDomAPI.js ***!
  \***************************************************************/
/***/ ((module) => {

eval("\n\n/* istanbul ignore next  */\nfunction apply(styleElement, options, obj) {\n  var css = \"\";\n\n  if (obj.supports) {\n    css += \"@supports (\".concat(obj.supports, \") {\");\n  }\n\n  if (obj.media) {\n    css += \"@media \".concat(obj.media, \" {\");\n  }\n\n  var needLayer = typeof obj.layer !== \"undefined\";\n\n  if (needLayer) {\n    css += \"@layer\".concat(obj.layer.length > 0 ? \" \".concat(obj.layer) : \"\", \" {\");\n  }\n\n  css += obj.css;\n\n  if (needLayer) {\n    css += \"}\";\n  }\n\n  if (obj.media) {\n    css += \"}\";\n  }\n\n  if (obj.supports) {\n    css += \"}\";\n  }\n\n  var sourceMap = obj.sourceMap;\n\n  if (sourceMap && typeof btoa !== \"undefined\") {\n    css += \"\\n/*# sourceMappingURL=data:application/json;base64,\".concat(btoa(unescape(encodeURIComponent(JSON.stringify(sourceMap)))), \" */\");\n  } // For old IE\n\n  /* istanbul ignore if  */\n\n\n  options.styleTagTransform(css, styleElement, options.options);\n}\n\nfunction removeStyleElement(styleElement) {\n  // istanbul ignore if\n  if (styleElement.parentNode === null) {\n    return false;\n  }\n\n  styleElement.parentNode.removeChild(styleElement);\n}\n/* istanbul ignore next  */\n\n\nfunction domAPI(options) {\n  var styleElement = options.insertStyleElement(options);\n  return {\n    update: function update(obj) {\n      apply(styleElement, options, obj);\n    },\n    remove: function remove() {\n      removeStyleElement(styleElement);\n    }\n  };\n}\n\nmodule.exports = domAPI;\n\n//# sourceURL=webpack://Tita/./node_modules/style-loader/dist/runtime/styleDomAPI.js?");

/***/ }),

/***/ "./node_modules/style-loader/dist/runtime/styleTagTransform.js":
/*!*********************************************************************!*\
  !*** ./node_modules/style-loader/dist/runtime/styleTagTransform.js ***!
  \*********************************************************************/
/***/ ((module) => {

eval("\n\n/* istanbul ignore next  */\nfunction styleTagTransform(css, styleElement) {\n  if (styleElement.styleSheet) {\n    styleElement.styleSheet.cssText = css;\n  } else {\n    while (styleElement.firstChild) {\n      styleElement.removeChild(styleElement.firstChild);\n    }\n\n    styleElement.appendChild(document.createTextNode(css));\n  }\n}\n\nmodule.exports = styleTagTransform;\n\n//# sourceURL=webpack://Tita/./node_modules/style-loader/dist/runtime/styleTagTransform.js?");

/***/ }),

/***/ "./src/app.js":
/*!********************!*\
  !*** ./src/app.js ***!
  \********************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

eval("__webpack_require__.r(__webpack_exports__);\n/* harmony import */ var _main_scss__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./main.scss */ \"./src/main.scss\");\n/* harmony import */ var _router_routes__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./router/routes */ \"./src/router/routes.js\");\n/* harmony import */ var _model_events__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./model/events */ \"./src/model/events.js\");\n\r\n\r\n\r\n\r\n\r\n\r\nlet events = new _model_events__WEBPACK_IMPORTED_MODULE_2__.Events();\r\n(0,_router_routes__WEBPACK_IMPORTED_MODULE_1__.router)(window.location.hash);\r\nwindow.addEventListener('hashchange', () => {\r\n    (0,_router_routes__WEBPACK_IMPORTED_MODULE_1__.router)(window.location.hash)\r\n});\r\nevents.toInit();\r\n\n\n//# sourceURL=webpack://Tita/./src/app.js?");

/***/ }),

/***/ "./src/controllers/all.js":
/*!********************************!*\
  !*** ./src/controllers/all.js ***!
  \********************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

eval("__webpack_require__.r(__webpack_exports__);\n/* harmony export */ __webpack_require__.d(__webpack_exports__, {\n/* harmony export */   \"All\": () => (/* binding */ All)\n/* harmony export */ });\n/* harmony import */ var _model_search__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../model/search */ \"./src/model/search.js\");\n\r\n\r\nclass All{\r\n     constructor(){\r\n        this.images = [];\r\n        this.buscador = new _model_search__WEBPACK_IMPORTED_MODULE_0__.Search();\r\n       \r\n   \r\n    \r\n    }\r\n    async show_all(section){\r\n        let all =[\"branding\",\"web\",\"photography\",\"app\"];\r\n\r\n        for(let i = 0; i < all.length; i++){\r\n           this.buscador.set_word_search(all[i]);\r\n           let aux_images= await this.get_posts_all();\r\n           if(aux_images){\r\n             this.images=this.images.concat(aux_images);\r\n           }\r\n        }\r\n\r\n    \r\n        let innerHTML=\"<div class='grid'>\";\r\n        this.images.map((item,index) => {\r\n         \r\n            innerHTML += `<div style=\"background-image: url('${item.urls.small}')\" id=\"item-${index+1}\">\r\n            <div class='show-msg'>\r\n            <h1>CREATIVE LOGO</h1>\r\n            <hr>\r\n            <p>All</p>\r\n            </div>\r\n            </div>`;\r\n        });\r\n        section.innerHTML = innerHTML+\"</div>\";\r\n        console.log(innerHTML);\r\n\r\n     \r\n       \r\n    \r\n\r\n    }\r\n    get_posts_all(){\r\n        return this.buscador.search().then(data => { \r\n             return data;\r\n        });\r\n    }    \r\n}\r\n\n\n//# sourceURL=webpack://Tita/./src/controllers/all.js?");

/***/ }),

/***/ "./src/controllers/app.js":
/*!********************************!*\
  !*** ./src/controllers/app.js ***!
  \********************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

eval("__webpack_require__.r(__webpack_exports__);\n/* harmony export */ __webpack_require__.d(__webpack_exports__, {\n/* harmony export */   \"default\": () => (__WEBPACK_DEFAULT_EXPORT__)\n/* harmony export */ });\n/* harmony import */ var _views_app_html__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../views/app.html */ \"./src/views/app.html\");\n/* harmony import */ var _model_search__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../model/search */ \"./src/model/search.js\");\n\r\n\r\n/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (()=>{\r\n    const div = document.createElement('div');\r\n    div.innerHTML = _views_app_html__WEBPACK_IMPORTED_MODULE_0__[\"default\"];\r\n    div.classList='grid';\r\n\r\n    let search = new _model_search__WEBPACK_IMPORTED_MODULE_1__.Search();\r\n    search.set_word_search('app');\r\n    search.get_posts().then(data => {\r\n        data.map((item,index) => {\r\n            console.log(item.urls.thumb);\r\n            div.innerHTML += `<div style=\"background-image: url('${item.urls.small}')\" id=\"item-${index+1}\">\r\n                                    <div class='show-msg'>\r\n                                        <h1>CREATIVE LOGO</h1>\r\n                                        <hr>\r\n                                        <p>app</p>\r\n                                    </div>\r\n                            </div>`;\r\n        })\r\n    })\r\n    return div;\r\n});\r\n\n\n//# sourceURL=webpack://Tita/./src/controllers/app.js?");

/***/ }),

/***/ "./src/controllers/branding.js":
/*!*************************************!*\
  !*** ./src/controllers/branding.js ***!
  \*************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

eval("__webpack_require__.r(__webpack_exports__);\n/* harmony export */ __webpack_require__.d(__webpack_exports__, {\n/* harmony export */   \"default\": () => (__WEBPACK_DEFAULT_EXPORT__)\n/* harmony export */ });\n/* harmony import */ var _views_branding_html__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../views/branding.html */ \"./src/views/branding.html\");\n/* harmony import */ var _model_search__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../model/search */ \"./src/model/search.js\");\n\r\n\r\n/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (()=>{\r\n    const div = document.createElement('div');\r\n    div.innerHTML = _views_branding_html__WEBPACK_IMPORTED_MODULE_0__[\"default\"];\r\n    div.classList='grid';\r\n\r\n    let search = new _model_search__WEBPACK_IMPORTED_MODULE_1__.Search();\r\n    search.set_word_search('branding');\r\n    search.get_posts().then(data => {\r\n        data.map((item,index) => {\r\n            console.log(item.urls.thumb);\r\n            div.innerHTML += `<div style=\"background-image: url('${item.urls.small}')\" id=\"item-${index+1}\">\r\n            <div class='show-msg'>\r\n                <h1>CREATIVE LOGO</h1>\r\n                <hr>\r\n                <p>branding</p>\r\n            </div>\r\n            </div>`;\r\n        })\r\n    })\r\n    return div;\r\n});\r\n\n\n//# sourceURL=webpack://Tita/./src/controllers/branding.js?");

/***/ }),

/***/ "./src/controllers/main.js":
/*!*********************************!*\
  !*** ./src/controllers/main.js ***!
  \*********************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

eval("__webpack_require__.r(__webpack_exports__);\n/* harmony export */ __webpack_require__.d(__webpack_exports__, {\n/* harmony export */   \"pages\": () => (/* binding */ pages)\n/* harmony export */ });\n/* harmony import */ var _app__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./app */ \"./src/controllers/app.js\");\n/* harmony import */ var _branding__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./branding */ \"./src/controllers/branding.js\");\n/* harmony import */ var _photography__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./photography */ \"./src/controllers/photography.js\");\n/* harmony import */ var _web__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./web */ \"./src/controllers/web.js\");\n/* harmony import */ var _all__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./all */ \"./src/controllers/all.js\");\n\r\n\r\n\r\n\r\n\r\n\r\n\r\nconst pages={\r\n    app: _app__WEBPACK_IMPORTED_MODULE_0__[\"default\"],\r\n    branding: _branding__WEBPACK_IMPORTED_MODULE_1__[\"default\"],\r\n    photography: _photography__WEBPACK_IMPORTED_MODULE_2__[\"default\"],\r\n    web: _web__WEBPACK_IMPORTED_MODULE_3__[\"default\"],\r\n    all: new _all__WEBPACK_IMPORTED_MODULE_4__.All()\r\n\r\n\r\n};\r\n\r\n\n\n//# sourceURL=webpack://Tita/./src/controllers/main.js?");

/***/ }),

/***/ "./src/controllers/photography.js":
/*!****************************************!*\
  !*** ./src/controllers/photography.js ***!
  \****************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

eval("__webpack_require__.r(__webpack_exports__);\n/* harmony export */ __webpack_require__.d(__webpack_exports__, {\n/* harmony export */   \"default\": () => (__WEBPACK_DEFAULT_EXPORT__)\n/* harmony export */ });\n/* harmony import */ var _views_branding_html__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../views/branding.html */ \"./src/views/branding.html\");\n/* harmony import */ var _model_search__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../model/search */ \"./src/model/search.js\");\n\r\n\r\n/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (()=>{\r\n    const div = document.createElement('div');\r\n    div.innerHTML = _views_branding_html__WEBPACK_IMPORTED_MODULE_0__[\"default\"];\r\n    div.classList='grid';\r\n\r\n    let search = new _model_search__WEBPACK_IMPORTED_MODULE_1__.Search();\r\n    search.set_word_search('photography');\r\n    search.get_posts().then(data => {\r\n        data.map((item,index) => {\r\n            console.log(item.urls.thumb);\r\n            div.innerHTML += `<div style=\"background-image: url('${item.urls.small}')\" id=\"item-${index+1}\">\r\n            <div class='show-msg'>\r\n                <h1>CREATIVE LOGO</h1>\r\n                <hr>\r\n                <p>Photography</p>\r\n            </div>\r\n            </div>`;\r\n        })\r\n    })\r\n    return div;\r\n    \r\n});\r\n\n\n//# sourceURL=webpack://Tita/./src/controllers/photography.js?");

/***/ }),

/***/ "./src/controllers/web.js":
/*!********************************!*\
  !*** ./src/controllers/web.js ***!
  \********************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

eval("__webpack_require__.r(__webpack_exports__);\n/* harmony export */ __webpack_require__.d(__webpack_exports__, {\n/* harmony export */   \"default\": () => (__WEBPACK_DEFAULT_EXPORT__)\n/* harmony export */ });\n/* harmony import */ var _views_web_html__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../views/web.html */ \"./src/views/web.html\");\n/* harmony import */ var _model_search__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../model/search */ \"./src/model/search.js\");\n\r\n\r\n/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (()=>{\r\n    const div = document.createElement('div');\r\n    div.innerHTML = _views_web_html__WEBPACK_IMPORTED_MODULE_0__[\"default\"];\r\n    div.classList='grid';\r\n\r\n    let search = new _model_search__WEBPACK_IMPORTED_MODULE_1__.Search();\r\n    search.set_word_search('web');\r\n    search.get_posts().then(data => {\r\n        data.map((item,index) => {\r\n            console.log(item.urls.thumb);\r\n            div.innerHTML += `<div style=\"background-image: url('${item.urls.small}')\" id=\"item-${index+1}\">\r\n            <div class='show-msg'>\r\n                <h1>CREATIVE LOGO</h1>\r\n                <hr>\r\n                <p>web</p>\r\n            </div>\r\n            </div>`;\r\n        })\r\n    })\r\n    return div;\r\n});\r\n\n\n//# sourceURL=webpack://Tita/./src/controllers/web.js?");

/***/ }),

/***/ "./src/model/events.js":
/*!*****************************!*\
  !*** ./src/model/events.js ***!
  \*****************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

eval("__webpack_require__.r(__webpack_exports__);\n/* harmony export */ __webpack_require__.d(__webpack_exports__, {\n/* harmony export */   \"Events\": () => (/* binding */ Events)\n/* harmony export */ });\nclass Events{\r\n    constructor(){\r\n        this.view2 = document.querySelectorAll(\"#view-2\");\r\n    }\r\n    toInit(){\r\n        document.getElementById(\"view-1\").addEventListener('click', () => {\r\n            document.querySelector('#sections div').classList.add('grid-cubic');\r\n            document.querySelector('#sections div').classList.remove('grid');\r\n        });\r\n        document.getElementById(\"view-2\").addEventListener('click', () => {\r\n            document.querySelector('#sections div').classList.add('grid');\r\n            document.querySelector('#sections div').classList.remove('grid-cubic');\r\n        });\r\n    }\r\n    \r\n}\r\n\n\n//# sourceURL=webpack://Tita/./src/model/events.js?");

/***/ }),

/***/ "./src/model/search.js":
/*!*****************************!*\
  !*** ./src/model/search.js ***!
  \*****************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

eval("__webpack_require__.r(__webpack_exports__);\n/* harmony export */ __webpack_require__.d(__webpack_exports__, {\n/* harmony export */   \"Search\": () => (/* binding */ Search)\n/* harmony export */ });\nconst clientId=\"DetHBKI3Rs-gNATnHnSKebYZLn9kakg63ZE6GbL598I\";\r\nconst endpoint =\"https://api.unsplash.com/search/photos\"\r\nclass Search{\r\n    constructor(){\r\n        this.word_search = \"\" ;\r\n        this.images=[];\r\n    }\r\n    set_word_search(word){\r\n        this.word_search = word;\r\n    }\r\n    async get_posts(){\r\n        this.images = await this.search();\r\n        return this.images;\r\n    }\r\n    search(){\r\n        return fetch(`${endpoint}?query=${this.word_search}&client_id=${clientId}`)\r\n        .then(response => response.json())\r\n        .then(data => {\r\n           \r\n            return data.results;\r\n        })\r\n        return f;\r\n    }\r\n    \r\n}\r\n\n\n//# sourceURL=webpack://Tita/./src/model/search.js?");

/***/ }),

/***/ "./src/router/routes.js":
/*!******************************!*\
  !*** ./src/router/routes.js ***!
  \******************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

eval("__webpack_require__.r(__webpack_exports__);\n/* harmony export */ __webpack_require__.d(__webpack_exports__, {\n/* harmony export */   \"router\": () => (/* binding */ router)\n/* harmony export */ });\n/* harmony import */ var _controllers_main__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../controllers/main */ \"./src/controllers/main.js\");\n\r\nconst section = document.getElementById('sections');\r\nclass Menu {\r\n    constructor() {\r\n        this.menu = document.querySelectorAll(\".menu li a\");\r\n    }\r\n    clean_items() {\r\n        this.menu.forEach(item => {\r\n            item.classList.remove(\"active\");\r\n        });\r\n    }\r\n    active_item(item) {\r\n        this.clean_items();\r\n        document.querySelectorAll(\"a[href='\" + item + \"']\").forEach(item => {\r\n            item.classList.add(\"active\");\r\n        })\r\n    }\r\n}\r\nconst router = (route) => {\r\n    section.innerHTML = \"\";\r\n    //limipiamos los links\r\n    let menu = new Menu();\r\n    menu.active_item(route);\r\n    switch (route) {\r\n        case '#all':\r\n            let all = _controllers_main__WEBPACK_IMPORTED_MODULE_0__.pages.all;\r\n            let html = all.show_all(section);\r\n            break;\r\n        case '#branding': {\r\n            return section.appendChild(_controllers_main__WEBPACK_IMPORTED_MODULE_0__.pages.branding());\r\n        }\r\n        case '#web': {\r\n            return section.appendChild(_controllers_main__WEBPACK_IMPORTED_MODULE_0__.pages.web());\r\n        }\r\n        case '#photography': {\r\n            return section.appendChild(_controllers_main__WEBPACK_IMPORTED_MODULE_0__.pages.photography());\r\n        }\r\n        case '#app': {\r\n            return section.appendChild(_controllers_main__WEBPACK_IMPORTED_MODULE_0__.pages.app());\r\n        }\r\n        default:\r\n            console.log('Pagina no encontrada 404');\r\n    }\r\n}\r\n\n\n//# sourceURL=webpack://Tita/./src/router/routes.js?");

/***/ }),

/***/ "./src/images/search_icon.png":
/*!************************************!*\
  !*** ./src/images/search_icon.png ***!
  \************************************/
/***/ ((module, __unused_webpack_exports, __webpack_require__) => {

eval("module.exports = __webpack_require__.p + \"b662f931f9505babb10c.png\";\n\n//# sourceURL=webpack://Tita/./src/images/search_icon.png?");

/***/ })

/******/ 	});
/************************************************************************/
/******/ 	// The module cache
/******/ 	var __webpack_module_cache__ = {};
/******/ 	
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/ 		// Check if module is in cache
/******/ 		var cachedModule = __webpack_module_cache__[moduleId];
/******/ 		if (cachedModule !== undefined) {
/******/ 			return cachedModule.exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = __webpack_module_cache__[moduleId] = {
/******/ 			id: moduleId,
/******/ 			// no module.loaded needed
/******/ 			exports: {}
/******/ 		};
/******/ 	
/******/ 		// Execute the module function
/******/ 		__webpack_modules__[moduleId](module, module.exports, __webpack_require__);
/******/ 	
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/ 	
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = __webpack_modules__;
/******/ 	
/************************************************************************/
/******/ 	/* webpack/runtime/compat get default export */
/******/ 	(() => {
/******/ 		// getDefaultExport function for compatibility with non-harmony modules
/******/ 		__webpack_require__.n = (module) => {
/******/ 			var getter = module && module.__esModule ?
/******/ 				() => (module['default']) :
/******/ 				() => (module);
/******/ 			__webpack_require__.d(getter, { a: getter });
/******/ 			return getter;
/******/ 		};
/******/ 	})();
/******/ 	
/******/ 	/* webpack/runtime/define property getters */
/******/ 	(() => {
/******/ 		// define getter functions for harmony exports
/******/ 		__webpack_require__.d = (exports, definition) => {
/******/ 			for(var key in definition) {
/******/ 				if(__webpack_require__.o(definition, key) && !__webpack_require__.o(exports, key)) {
/******/ 					Object.defineProperty(exports, key, { enumerable: true, get: definition[key] });
/******/ 				}
/******/ 			}
/******/ 		};
/******/ 	})();
/******/ 	
/******/ 	/* webpack/runtime/global */
/******/ 	(() => {
/******/ 		__webpack_require__.g = (function() {
/******/ 			if (typeof globalThis === 'object') return globalThis;
/******/ 			try {
/******/ 				return this || new Function('return this')();
/******/ 			} catch (e) {
/******/ 				if (typeof window === 'object') return window;
/******/ 			}
/******/ 		})();
/******/ 	})();
/******/ 	
/******/ 	/* webpack/runtime/hasOwnProperty shorthand */
/******/ 	(() => {
/******/ 		__webpack_require__.o = (obj, prop) => (Object.prototype.hasOwnProperty.call(obj, prop))
/******/ 	})();
/******/ 	
/******/ 	/* webpack/runtime/make namespace object */
/******/ 	(() => {
/******/ 		// define __esModule on exports
/******/ 		__webpack_require__.r = (exports) => {
/******/ 			if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 				Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 			}
/******/ 			Object.defineProperty(exports, '__esModule', { value: true });
/******/ 		};
/******/ 	})();
/******/ 	
/******/ 	/* webpack/runtime/publicPath */
/******/ 	(() => {
/******/ 		var scriptUrl;
/******/ 		if (__webpack_require__.g.importScripts) scriptUrl = __webpack_require__.g.location + "";
/******/ 		var document = __webpack_require__.g.document;
/******/ 		if (!scriptUrl && document) {
/******/ 			if (document.currentScript)
/******/ 				scriptUrl = document.currentScript.src
/******/ 			if (!scriptUrl) {
/******/ 				var scripts = document.getElementsByTagName("script");
/******/ 				if(scripts.length) scriptUrl = scripts[scripts.length - 1].src
/******/ 			}
/******/ 		}
/******/ 		// When supporting browsers where an automatic publicPath is not supported you must specify an output.publicPath manually via configuration
/******/ 		// or pass an empty string ("") and set the __webpack_public_path__ variable from your code to use your own logic.
/******/ 		if (!scriptUrl) throw new Error("Automatic publicPath is not supported in this browser");
/******/ 		scriptUrl = scriptUrl.replace(/#.*$/, "").replace(/\?.*$/, "").replace(/\/[^\/]+$/, "/");
/******/ 		__webpack_require__.p = scriptUrl;
/******/ 	})();
/******/ 	
/******/ 	/* webpack/runtime/jsonp chunk loading */
/******/ 	(() => {
/******/ 		__webpack_require__.b = document.baseURI || self.location.href;
/******/ 		
/******/ 		// object to store loaded and loading chunks
/******/ 		// undefined = chunk not loaded, null = chunk preloaded/prefetched
/******/ 		// [resolve, reject, Promise] = chunk loading, 0 = chunk loaded
/******/ 		var installedChunks = {
/******/ 			"main": 0
/******/ 		};
/******/ 		
/******/ 		// no chunk on demand loading
/******/ 		
/******/ 		// no prefetching
/******/ 		
/******/ 		// no preloaded
/******/ 		
/******/ 		// no HMR
/******/ 		
/******/ 		// no HMR manifest
/******/ 		
/******/ 		// no on chunks loaded
/******/ 		
/******/ 		// no jsonp function
/******/ 	})();
/******/ 	
/************************************************************************/
/******/ 	
/******/ 	// startup
/******/ 	// Load entry module and return exports
/******/ 	// This entry module can't be inlined because the eval devtool is used.
/******/ 	var __webpack_exports__ = __webpack_require__("./src/app.js");
/******/ 	
/******/ })()
;